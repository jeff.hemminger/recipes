-- Deploy recipes:ingredients to pg
-- requires: appschema
BEGIN;

CREATE OR REPLACE FUNCTION recipes.can_insert_group_member(gid integer)
RETURNS BOOLEAN AS $$
DECLARE
    is_owner BOOLEAN;
    is_created BOOLEAN;
BEGIN


    SELECT EXISTS INTO is_owner (SELECT true FROM recipes.groups_members
      WHERE group_id = gid
      AND username=current_user
      AND owner=true);

    SELECT EXISTS INTO is_created (SELECT true FROM recipes.groups
      WHERE created_by=current_user
      AND id=gid);

    RETURN is_owner OR is_created;

END;
$$ LANGUAGE plpgsql;	

CREATE OR REPLACE FUNCTION recipes.can_update_group_or_member(gid integer)
RETURNS BOOLEAN AS $$
DECLARE
  is_owner BOOLEAN;
BEGIN

  SELECT EXISTS INTO is_owner (SELECT true FROM recipes.groups_members
    WHERE group_id = gid
    AND username=current_user
    AND owner=true);

  RETURN is_owner;
END;
$$ LANGUAGE plpgsql;
 
-- all users can see all groups
CREATE POLICY select_group_policy ON recipes.groups FOR SELECT USING (true);

-- all users can create a group
CREATE POLICY insert_group_policy ON recipes.groups FOR INSERT WITH CHECK(true);

-- only owners can update a group
CREATE POLICY update_group_policy ON recipes.groups
FOR UPDATE USING (recipes.can_update_group_or_member(id));

-- all users can see all group members
CREATE POLICY select_groups_members ON recipes.groups_members FOR SELECT USING (true);

-- all users can create group members if they are owners
CREATE POLICY insert_groups_members ON recipes.groups_members FOR INSERT WITH CHECK
  (recipes.can_insert_group_member(group_id));

-- all users can update group members if they are owners
CREATE POLICY update_groups_members ON recipes.groups_members FOR UPDATE USING (recipes.can_update_group_or_member(group_id));

--CREATE POLICY administrator_all ON recipes.groups USING (true) WITH CHECK (true);
--CREATE POLICY admin_all ON recipes.groups_members USING (true) WITH CHECK (true);


COMMIT;
