-- Deploy recipes:ingredients to pg
-- requires: appschema
BEGIN;

CREATE TABLE recipes.groups (
    id SERIAL PRIMARY KEY,
    name VARCHAR(20) NOT NULL,
    description TEXT,
    created_by TEXT
);


ALTER TABLE recipes.groups ENABLE ROW LEVEL SECURITY;

CREATE OR REPLACE FUNCTION recipes.update_created_by()
RETURNS TRIGGER 
AS 
$$
BEGIN
    NEW.created_by := current_user;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION recipes.add_owner_row()
RETURNS TRIGGER 
AS 
$$
BEGIN
    INSERT INTO recipes.groups_members (group_id, username, owner) VALUES (NEW.id, current_user, true);
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER update_created_by BEFORE INSERT ON recipes.groups
  FOR EACH ROW
  EXECUTE FUNCTION recipes.update_created_by();

CREATE TRIGGER add_owner_row AFTER INSERT ON recipes.groups
  FOR EACH ROW
  EXECUTE FUNCTION recipes.add_owner_row();


COMMIT;
