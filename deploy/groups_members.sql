-- Deploy recipes:ingredients to pg
-- requires: appschema
BEGIN;

CREATE TABLE recipes.groups_members (
    group_id int NOT NULL,
    username TEXT NOT NULL,
    owner boolean NOT NULL default false
);

ALTER TABLE recipes.groups_members ENABLE ROW LEVEL SECURITY;

COMMIT;
