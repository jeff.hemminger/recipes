-- Deploy recipes:roles to pg

BEGIN;

create role authenticator noinherit login password 'ydab2d';
create role guest nologin noinherit;
create role administrator nologin noinherit;
create role web_anon nologin;
grant web_anon to authenticator;



grant guest to authenticator;
grant administrator to authenticator;

create role testera nologin inherit;
create role testerb nologin inherit;
create role admin1 nologin inherit;

grant testera to guest;
grant testerb to guest;
grant admin1 to administrator;
grant testera to authenticator;
grant testerb to authenticator;
grant administrator to authenticator;

grant usage on schema recipes to web_anon;
grant usage on schema recipes to guest;
grant usage on schema recipes to administrator;
grant usage on schema recipes to testera;
grant usage on schema recipes to testerb;

grant select on recipes.steps_view to guest;
grant select on recipes.steps_view to testera;
grant select on recipes.steps_view to testerb;
grant select on recipes.steps_view to admin1;
grant select on recipes.steps to guest;
grant select on recipes.steps to testera;
grant select on recipes.steps to testerb;
grant select on recipes.steps to admin1;
grant select on recipes.recipes to guest;
grant select on recipes.recipes to testera;
grant select on recipes.recipes to testerb;
grant select on recipes.recipes to admin1;
grant select on recipes.recipe_ingredients to guest;
grant select on recipes.recipe_ingredients to testera;
grant select on recipes.recipe_ingredients to testerb;
grant select on recipes.recipe_ingredients to admin1;
grant select on recipes.ingredients to guest;
grant select on recipes.ingredients to testera;
grant select on recipes.ingredients to testerb;
grant select on recipes.ingredients to admin1;
grant select on recipes.ingredients_view to guest;
grant select on recipes.ingredients_view to testera;
grant select on recipes.ingredients_view to testerb;
grant select on recipes.ingredients_view to admin1;
grant select, update, insert  on recipes.groups to guest;
grant select, update, insert  on recipes.groups to testera;
grant select, update, insert  on recipes.groups to testerb;
grant select, update, insert  on recipes.groups to admin1;
grant select, update, insert  on recipes.groups_members to guest;
grant select, update, insert  on recipes.groups_members to testera;
grant select, update, insert  on recipes.groups_members to testerb;
grant select, update, insert  on recipes.groups_members to admin1;


grant select on recipes.steps_view to administrator;
grant select on recipes.steps to administrator;
grant select on recipes.recipes to administrator;
grant select on recipes.recipe_ingredients to administrator;
grant select on recipes.ingredients to administrator;
grant select on recipes.ingredients_view to administrator;
grant all  on recipes.groups to administrator;
grant all on recipes.groups_members to administrator;

GRANT USAGE, SELECT ON SEQUENCE recipes.groups_id_seq TO guest;
GRANT USAGE, SELECT ON SEQUENCE recipes.groups_id_seq TO administrator;
GRANT USAGE, SELECT ON SEQUENCE recipes.groups_id_seq TO testera;
GRANT USAGE, SELECT ON SEQUENCE recipes.groups_id_seq TO testerb;
GRANT USAGE, SELECT ON SEQUENCE recipes.groups_id_seq TO admin1;
COMMIT;
