import pytest
from httpx import AsyncClient

from .main import app


@pytest.mark.anyio
async def test_root():
    async with AsyncClient(app=app, base_url="http://localhost:5000") as ac:
        response = await ac.get("/")
    assert response.status_code == 200
    assert response.json() == {'admin_email': 'testinger', 'app_name': 'testtestest', 'items_per_user': 50}