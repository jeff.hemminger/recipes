from typing import Union, Annotated

from fastapi import FastAPI, File, UploadFile, Header, Depends

from functools import lru_cache
import jwt

import zipfile
import io
import os
from logging.config import dictConfig
import logging
from sqlalchemy.orm import Session

from .config import Settings, LogConfig
from .database import SessionLocal, engine, RecipeBase, Recipe


dictConfig(LogConfig().dict())
logger = logging.getLogger("mycoolapp")


app = FastAPI()

@lru_cache
def get_settings():
    return Settings()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/")
def read_root(settings: Annotated[Settings, Depends(get_settings)]):
    logger.info("logging from the root logger")

    return {
        "app_name": settings.app_name,
        "admin_email": settings.admin_email,
        "items_per_user": settings.items_per_user,
    }


@app.get("/items/")
async def read_items(authorization: Annotated[str | None, Header()]):
    logger.info("logging from the root logger")

    v = jwt.decode(authorization, "12345678910", algorithms=["HS256"])
    return {"Authorization": v['name']}


@app.post("/files/")
async def create_file(file: Annotated[bytes, File()]):
    try:
        z = zipfile.ZipFile(io.BytesIO(file))
        z.extractall()
        print(os.listdir('.'))
        z.close()
    except zipfile.BadZipFile:
        return {'error': 'Bad Zip File'}
    return {"file_size": len(file)}

def get_user(db: Session, recipe_id: int):
    return db.query(Recipe).filter(Recipe.id == recipe_id).first()


@app.post("/uploadfile/", response_model=RecipeBase)
async def create_upload_file(file: UploadFile, db: Session = Depends(get_db)):

    recipe = get_user(db, recipe_id=1)
    return recipe