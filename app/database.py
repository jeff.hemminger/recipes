from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String

from pydantic import BaseModel

DATABASE_URL = "postgresql://dbuser:password@db/recipes"
SCHEMA = 'recipes'


engine = create_engine(DATABASE_URL, echo=True, connect_args={'options': '-csearch_path={}'.format(SCHEMA)})
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()


class Recipe(Base):

    __tablename__ = "recipes"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, index=True)
    description = Column(String, unique=True, index=True)


class RecipeBase(BaseModel):
    id: int
    name: str
    description: str | None = None

    class Config:
        orm_mode = True