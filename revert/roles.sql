-- Revert recipes:roles

BEGIN;

DROP OWNED BY guest;
DROP OWNED BY administrator;
DROP OWNED BY web_anon;
DROP OWNED BY testera;
DROP OWNED BY testerb;
DROP OWNED BY admin1;

DROP ROLE IF EXISTS authenticator;
DROP ROLE IF EXISTS web_anon;
DROP ROLE IF EXISTS guest;
DROP ROLE IF EXISTS administrator;
DROP ROLE IF EXISTS testera;
DROP ROLE IF EXISTS testerb;
DROP ROLE IF EXISTS admin1;

COMMIT;
