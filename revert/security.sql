-- Revert recipes:ingredients
-- requires: appschema
BEGIN;

DROP FUNCTION recipes.can_insert_group_member CASCADE;
DROP FUNCTION recipes.can_update_group_or_member CASCADE;
COMMIT;
