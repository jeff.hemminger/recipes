-- Revert recipes:ingredients
-- requires: appschema
BEGIN;

DROP TABLE recipes.groups;

COMMIT;
