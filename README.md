# Recipez

## To Start

`docker-compose up -d` to run both the db and fastapi uvicorn server.

`docker-compose run sqitch sqitch deploy` should deploy the recipe schema and some data to postgres.

Assuming that runs without error, we will want to verify the deployment:

`docker-compose run sqitch sqitch verify` will run tests against the schema, making sure tables and views are there.

